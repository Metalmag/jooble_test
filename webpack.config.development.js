const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');

const port = process.env.PORT || 3000;
module.exports = {
  entry: ['babel-polyfill', 'react-hot-loader/patch', './src/index.js'],
  mode: 'development',
  watch: true,
  output: {
    filename: '[name].[hash].js',
    publicPath: '/',
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              sourceMap: false,
              localIdentName: '[local]_[hash:base64:5]',
            },
          },
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
    ],
  },
  devServer: {
    port,
    historyApiFallback: true,
    open: true,
    hot: true,
    disableHostCheck: false,
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebPackPlugin({
      template: 'public/index.html',
      filename: './index.html',
    }),
  ],
};
