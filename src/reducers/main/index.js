import { createReducer } from '../../utils/';
import t from './types';

// initial State
const initialState = {
  data: {},
};

export default createReducer(initialState, {
  [`${t.GET_ME}_REQUEST`](state) {
    return {
      ...state,
      isLoading: true,
    };
  },
  [`${t.GET_ME}_SUCCESS`](state, action) {
    return {
      ...state,
      isLoading: false,
      isLoadedQues: true,
      data: action.payload,
    };
  },
  [`${t.GET_ME}_FAILURE`](state, action) {
    return {
      ...state,
      isLoaded: false,
      isLoading: false,
      error: action.payload,
    };
  },
});
