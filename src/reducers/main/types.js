const prefix = '@main';

export default {
  GET_ME: `${prefix}/GET_ME`,
};
