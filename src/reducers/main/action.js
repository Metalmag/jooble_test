import t from './types';
import Api from '../../api/index';

// https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}

export const getME = (city) => ({
  type: t.GET_ME,
  payload: Api().get(city),
});