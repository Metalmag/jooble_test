import React from 'react';
import { compose, pure, lifecycle } from 'recompose';
import { connect } from 'react-redux'

import { getME } from '../reducers/main/action';

const HOC = compose(
    connect(
        ({ main }) => ({

            filters: main.filters,

        }),
        {
            getME,
        },
    ),

    lifecycle({
        componentDidMount() {
            this.props.getME('kiev');
        }
    }),
    pure, );

const App = HOC(({ }) => <div>123</div>);


export default App;