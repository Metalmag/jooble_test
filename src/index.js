import { AppContainer } from 'react-hot-loader';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store';

import App from './components/App';
const store = configureStore();


const renderApp = Component =>
  render(
    <AppContainer>
      <Provider store={store}>
        <Component />
      </Provider>
    </AppContainer>,
    document.getElementById('root'),
  );
renderApp(App);
// Webpack Hot Module Replacement API
if (module.hot) module.hot.accept('./components/App', () => render(App));
