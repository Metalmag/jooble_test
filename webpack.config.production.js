const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'production',
  cache: false,
  entry: ['babel-polyfill', './src/index.js'],
  optimization: {
    runtimeChunk: 'multiple',
    namedModules: true,
    splitChunks: false,
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: _publicPath,
    filename: '[name].js',
    chunkFilename: '[name].[chunkhash].js',
    hotUpdateChunkFilename: '[id].[hash].hot-update.js',
    hotUpdateMainFilename: '[hash].hot-update.json',
  },
  performance: { hints: false },
  stats: {
    entrypoints: true,
    children: false,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
        options: {
          compact: true,
          cacheDirectory: true,
        },
      },

      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[hash:base64:5]',
              camelCase: true,
              sourceMap: false,
              minimize: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                ctx: {
                  autoprefixer: {
                    browsers: 'last 2 versions',
                  },
                },
              },
            },
          },
        ],
      },
      {
        exclude: [
          /\.html$/,
          /\.(js|jsx)$/,
          /\.css$/,
          /\.json$/,
          /\.bmp$/,
          /\.gif$/,
          /\.jpe?g$/,
          /\.png$/,
          /\.svg/,
        ],
        loader: require.resolve('file-loader'),
        options: {
          name: 'src/img/[name].[ext]',
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      title: 'Code Splitting',
    }),
    new MiniCssExtractPlugin({
      filename: '[name]_[hash].css',
      chunkFilename: 'src/style/[name]_[hash].css',
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
  ],
  resolve: {
    modules: ['node_modules'],
    descriptionFiles: ['package.json'],
    extensions: ['.js', '.jsx', '.json'],
  },
  node: {
    fs: 'empty',
    vm: 'empty',
    net: 'empty',
    tls: 'empty',
  },
};
